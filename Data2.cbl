       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA2.
       AUTHOR. VIRAPAT.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  AL-NUM             PICTURE  X(5)  VALUE '1234'.
       01  NUM-INT            PICTURE  9(5).
       01  NUM-NOM-INT        PICTURE  9(3)V9(2).
       01  ALPHA              PICTURE  A(5).
       PROCEDURE DIVISION.
       BeginProgram.
           MOVE  AL-NUM TO NUM-INT
           DISPLAY NUM-INT   
           MOVE  AL-NUM TO NUM-NOM-INT  
           DISPLAY NUM-NOM-INT   
           MOVE  AL-NUM  TO ALPHA
           DISPLAY  ALPHA 
       .
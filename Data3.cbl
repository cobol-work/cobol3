       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DATA3.
       AUTHOR. Virapat.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  SURNAME           PICTURE X(8)      VALUE 'COUGHLAN'.
       01  SALE-PRICE        PICTURE 9(4)V9(2).
       01  NUM-OF-EMPLOYEES  PICTURE 999V99.
       01  SALARY            PICTURE 9999V99.
       01  COUNTRY           PICTURE X(9).

       PROCEDURE DIVISION.
       BEGINPROGRAM.
           DISPLAY '1 ' SURNAME.
           MOVE 'SMITH' TO SURNAME.
           DISPLAY '2 ' SURNAME.
           MOVE 'FITZWILLIAM' TO SURNAME.
           DISPLAY '3 ' SURNAME
           .

           DISPLAY '1 ' SALE-PRICE
           MOVE ZEROS TO SALE-PRICE
           DISPLAY '2 ' SALE-PRICE
           MOVE 25.5 TO SALE-PRICE 
           DISPLAY '3 ' SALE-PRICE
           MOVE 7.553 TO SALE-PRICE 
           DISPLAY '4 ' SALE-PRICE
           .

           DISPLAY '1 ' NUM-OF-EMPLOYEES 
           MOVE 12.4 TO NUM-OF-EMPLOYEES 
           DISPLAY '2 ' NUM-OF-EMPLOYEES 
           MOVE 1745 TO NUM-OF-EMPLOYEES 
           DISPLAY '3 ' NUM-OF-EMPLOYEES

           MOVE  NUM-OF-EMPLOYEES  TO SALARY 
           DISPLAY SALARY 
           .

           MOVE 'GALWAY' TO COUNTRY 
           DISPLAY COUNTRY 
           MOVE ALL '@' TO COUNTRY 
           DISPLAY COUNTRY 

           .
      